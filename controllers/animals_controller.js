var express = require('express');
var router = express.Router();
var cat = require('../models/cat');
var dog = require('../models/dog');

// getting data from both cat & dog tables
router.get('/', (req, res) => {
    var animals = [];
    // request to cats table; add to animals array
    cat.all(catData => {
        catData.forEach(cat => {animals.push(cat)});
    });   

    // request to dogs table; add to animals array
    dog.all(dogData => {
        dogData.forEach(dog => {animals.push(dog)});
        res.render('index', {animals: animals});
    });
});

// adding pet data; pet type determined by param
router.post('/api/pets/:pet', (req, res) => {
    // inserting pet details
    if (req.params.pet === 'cat'){
        cat.add(
            [
                req.body.pet_name,
                req.body.is_hungry,
                req.body.num_toys,
                req.body.is_cat
            ],
            function(data){
                // sends id back
                res.json({id: data.insertId});
            }
        );
    } else if (req.params.pet === 'dog'){
        dog.add(
            [
                req.body.pet_name,
                req.body.is_hungry,
                req.body.num_toys,
                req.body.is_cat
            ],
            function(data){
                // sends id back
                res.json({id: data.insertId});
            }
        );
    }
});

// updating pet data; pet in question determined by id
router.put('/api/pets/:pet/:id', (req, res) => {
    // updating specified column
    if (req.params.pet === 'cat'){
        cat.update(
            [
                req.body.column,
                req.body.columnVal,
                'id',
                req.params.id
            ],
            function(data){
                if (data.changedRows === 0) {
                    return res.status(404).end();
                }
                res.status(200).end();
            }
        );
    } else if (req.params.pet === 'dog'){
        dog.update(
            [
                req.body.column,
                parseInt(req.body.columnVal),
                'id',
                parseInt(req.params.id)
            ],
            function(data){
                if (data.changedRows === 0) {
                    return res.status(404).end();
                }
                res.status(200).end();
            }
        );
    }
});

module.exports = router;