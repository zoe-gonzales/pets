var dotenv = require('dotenv');
dotenv.config({path: '../.env'});

var mysql = require('mysql');
var connection;

if (process.env.JAWSDB_URL){
    connection = mysql.createConnection(process.env.JAWSDB_URL);
} else {
    connection = mysql.createConnection({
        host: 'localhost',
        port: 3306,
        user: 'root',
        password: process.env.PASS,
        database: 'pets_db'
    });
}

connection.connect(
    console.log('connection created')
);

module.exports = connection;