var connection  = require('./connection');

var orm = {
    selectAll: function(table, cb){
        var queryURL = `SELECT * FROM ${table}`;
        
        connection.query(
            queryURL,
            function(error, result){
                if (error) throw error;
                cb(result);
            }
        );
    },
    addOne: function(table, cols, vals, cb){
        var queryURL =  `INSERT INTO ${table} (??, ??, ??, ??) VALUES (?, ?, ?, ?)`
        var queryArr = [];
        cols.forEach(col => {queryArr.push(col)});
        vals.forEach(val => {queryArr.push(val)});

        connection.query(
            queryURL,
            queryArr,
            function(error, result){
                if (error) throw error;
                cb(result);
            }
        );
    },
    updateOne: function(table, input, cb){
        var queryURL = `UPDATE ${table} SET ?? = ? WHERE ?? = ?`
        console.log(input);
        connection.query(
            queryURL,
            input,
            function(error, result){
                if (error) throw error;
                cb(result);
            }
        );
    }
}

module.exports = orm;