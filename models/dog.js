var orm = require('../config/orm');

var dog = {
    all: function(cb){
        orm.selectAll('dogs', res => {
            cb(res);
        });
    },
    add: function(vals, cb){
        var columns = ['pet_name', 'is_hungry', 'num_toys', 'is_cat'];
        orm.addOne('dogs', columns, vals, res => {
            cb(res);
        });
    },
    update: function(input, cb){
        orm.updateOne('dogs', input, res => {
            cb(res);
        });
    }
}

module.exports = dog;