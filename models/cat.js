var orm = require('../config/orm');

var cat = {
    all: function(cb){
        orm.selectAll('cats', res => {
            cb(res);
        });
    },
    add: function(vals, cb){
        var columns = ['pet_name', 'is_hungry', 'num_toys', 'is_cat'];
        orm.addOne('cats', columns, vals, res => {
            cb(res);
        });
    },
    update: function(input, cb){
        orm.updateOne('cats', input, res => {
            cb(res);
        });
    }
}

module.exports = cat;