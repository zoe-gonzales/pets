INSERT INTO cats (pet_name, is_hungry, num_toys, is_cat)
VALUES
('Rosebud', false, 3, true),
('Basil', false, 4, true),
('Lilly', false, 2, true),
('Poppy', false, 6, true),
('Milo', false, 1, true);


INSERT INTO dogs (pet_name, is_hungry, num_toys, is_cat)
VALUES
('Evie', false, 7, false),
('Stella', false, 4, false),
('Roo', false, 2, false),
('Joey', false, 6, false),
('Otis', false, 1, false);