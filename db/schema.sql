CREATE DATABASE pets_db;

USE pets_db;

CREATE TABLE cats (
    id INT AUTO_INCREMENT NOT NULL,
    pet_name VARCHAR(50),
    is_hungry BOOLEAN,
    num_toys INT(10),
    is_cat BOOLEAN,
    PRIMARY KEY(id)
);

CREATE TABLE dogs (
    id INT AUTO_INCREMENT NOT NULL,
    pet_name VARCHAR(50),
    is_hungry BOOLEAN,
    num_toys INT(10),
    is_cat BOOLEAN,
    PRIMARY KEY(id)
);