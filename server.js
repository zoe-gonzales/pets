var express = require('express');
var app = express();

// port app will listen on
var PORT = process.env.PORT || 3000;

// serving static files
app.use(express.static("public"));

// parsing content
app.use(express.urlencoded({ extended: true }));
app.use(express.json());

// handlebars
var exphbs = require("express-handlebars");

// handlebars is the view engine
app.engine("handlebars", exphbs({ defaultLayout: "main" }));
app.set("view engine", "handlebars");

// express routes
var router = require('./controllers/animals_controller');

// using these routes in app
app.use(router);

app.listen(PORT, function(){
    console.log(`listening on port ${PORT}`);
});