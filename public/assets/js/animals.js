$(document).ready(function(){
    // listener that registers click on add pet button
    $('#add-pet').on('submit', event => {
        event.preventDefault();
        
        var pet;
        if ($('[name=is_cat]:checked').val() === 'on'){
            pet = 'cat';
        } else pet = 'dog';

        var hungry;
        if ($('[name=hungry]:checked').val() === 'on'){
            hungry = 1;
        } else hungry = 0;

        var cat;
        if ($('[name=is_cat]:checked').val() === 'on'){
            cat = 1;
        } else cat = 0;

        var newPet = {
            pet_name: $('[name=name]').val().trim(),
            is_hungry: hungry,
            num_toys: parseInt($('[name=toys]').val()),
            is_cat: cat
        }

        $.ajax('/api/pets/' + pet, {
            method: 'POST',
            data: newPet
        }).then(function(){
            location.reload();
        });

    });

    // listener that registers click on Feed/Play button
    $(document).on('click', '.is_hungry', function(){
        var id = $(this).attr('data-id');
        var column = 'is_hungry';
        var columnVal;
        if ($(this).attr('data-hungry') === '0'){
            columnVal = 0;
        } else columnVal = 1;

        var pet;
    
        if ($(this).attr('data-pet') === '1'){
            pet = 'cat';
        } else pet = 'dog';

        var updatedPet = {
            column: column,
            columnVal: columnVal,
            id: id
        }

        $.ajax(`/api/pets/${pet}/${id}`, {
            method: 'PUT',
            data: updatedPet
        }).then(
            function(){
            location.reload();
        });
    });

    // listener that registers click on toy button
    function alterToys(element, operator){
        $(document).on('click', element, function(){
            var id = $(this).attr('data-id');
            var column = 'num_toys';
            var columnVal = parseInt($(this).attr('data-toys'));

            if (operator === '+'){
                columnVal++;
            } else if (operator === '-'){
                columnVal--;
            }
    
            var pet;
            if ($(this).attr('data-pet') === '1'){
                pet = 'cat';
            } else pet = 'dog';
    
            var updatedPet = {
                column: column,
                columnVal: columnVal,
                id: id
            }

            $.ajax(`/api/pets/${pet}/${id}`, {
                method: 'PUT',
                data: updatedPet
            }).then(
                function(){
                location.reload();
            });
        });
    }

    alterToys('.add-toy', '+');
    alterToys('.remove-toy', '-')
});
