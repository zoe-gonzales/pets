# pets

### Summary
Simple app designed using the MVC pattern. Player can add cats and dogs to the page and change details related to each pet such as whether or not they're hungry and how many toys they have. This site was made as practice with a custom ORM, Express, and SQL queries. All data is stored and retrieved from a MySQL database.

### Built with
**Model/Controller:** Node, Express, MySQL, SQL <br>
**View:** Handlebars, jQuery, Bootstrap, custom CSS, Google Fonts, Subtle Patterns (background image)

### To use
Navigate to the app's [homepage](https://quiet-dusk-38184.herokuapp.com/).

Click on any of the buttons to change that pet's data, or add your own pet via the form at the bottom.

![Pets Homepage](./images/pets.png)